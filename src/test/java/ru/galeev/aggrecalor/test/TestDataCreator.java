package ru.galeev.aggrecalor.test;

import org.springframework.stereotype.Service;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.entities.Product;
import ru.galeev.aggrecalor.entities.requests.ImportRequest;

import java.util.*;

@Service
public class TestDataCreator {

	private static final String CATEGORY_NAME = "categoryName";
	private static final String NAME = "optimum nutrition";


	public ImportRequest createImportRequest() {
		ImportRequest request = new ImportRequest();
		request.setCategoryName(CATEGORY_NAME);
		Set<Product> products = new HashSet<Product>();
		Collections.addAll(products, createRandomProduct(), createRandomProduct(), createRandomProduct());
		request.setProducts(products);
		return request;
	}

	public Product createRandomProduct() {
		Product product = new Product();
		product.setName(generateString());
		product.setProteins(Math.random());
		product.setKilocalories(Math.random());
		product.setFats(Math.random());
		product.setCarbohydrates(Math.random());
		return product;
	}

    public Category createRandomCategory() {
        Category category = new Category();
        category.setName(generateString());
        return category;
    }

	public String generateString() {
		int length = 50;
		Random random = new Random();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = NAME.charAt(random.nextInt(NAME.length()));
		}
		return new String(text);
	}
}

package ru.galeev.aggrecalor.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TestDataPersistence {

    private static final String SELECT_ALL_CATEGORIES = "Select c from Category c";
    private static final String SELECT_ALL_FROM_PRODUCT = "Select c from Product c";

    @Autowired
    private TestDataCreator creator;

    @PersistenceContext
    private EntityManager manager;

    public List<Category> persistCategories(int count) {
        if (count <= 0) {
            return null;
        }
        List<Category> result = new ArrayList<Category>();
        for(int i = 0; i < count; i++) {
            Category category = creator.createRandomCategory();
            manager.persist(category);
            result.add(category);
        }
        manager.flush();
        return result;
    }

    public Category persistCategory() {
        Category category = creator.createRandomCategory();
        manager.persist(category);
        manager.flush();
        return category;
    }

    public List<Category> getAllCategories() {
        return manager.createQuery(SELECT_ALL_CATEGORIES, Category.class).getResultList();
    }

    public List<Product> persistProducts(int count) {
        if (count <= 0) {
            return null;
        }
        List<Product> result = new ArrayList<Product>();
        Category category = persistCategory();
        for(int i = 0; i < count; i++) {
            Product product = creator.createRandomProduct();
			product.setImported(false);
			product.setCreationDate(new Date());
            product.setCategory(category);
            manager.persist(product);
            result.add(product);
        }
        manager.flush();
        return result;
    }

    public Product persistProduct() {
        Product product = creator.createRandomProduct();
		product.setImported(false);
		product.setCreationDate(new Date());
        Category category = persistCategory();
        product.setCategory(category);
        manager.persist(product);
        manager.flush();
        return product;
    }

    public List<Product> getAllProducts() {
        return manager.createQuery(SELECT_ALL_FROM_PRODUCT, Product.class).getResultList();
    }
}

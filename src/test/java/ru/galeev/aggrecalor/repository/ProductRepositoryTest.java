package ru.galeev.aggrecalor.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.entities.Product;
import ru.galeev.aggrecalor.entities.requests.ImportRequest;
import ru.galeev.aggrecalor.entities.validators.EntityValidator;
import ru.galeev.aggrecalor.test.TestDataCreator;
import ru.galeev.aggrecalor.test.TestDataPersistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(locations = {"classpath:test-spring-config.xml"})
public class ProductRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private TestDataCreator creator;

    @Autowired
    private EntityValidator validator;

    @Autowired
    private TestDataPersistence testDataPersistence;

    @Test
    public void testCreate() throws Exception {
        Product product = creator.createRandomProduct();
        Product result = repository.create(product);
        assertThat(result, is(testDataPersistence.getAllProducts().get(0)));
    }

    @Test
    public void testUpdate() throws Exception {
        Product product = testDataPersistence.persistProduct();
        product.setName("TEST");
        Product result = repository.update(product);
        assertThat(result, is(product));
    }

    @Test
    public void testDelete() throws Exception {
        Product product = testDataPersistence.persistProduct();
        repository.delete(product);
        assertThat(0, is(testDataPersistence.getAllProducts().size()));
    }

	@Test
	public void testDeleteById() throws Exception {
		Product product = testDataPersistence.persistProduct();
		repository.delete(product.getId());
		assertThat(0, is(testDataPersistence.getAllProducts().size()));
	}

	@Test
	public void testGet() throws Exception {
		Product expected = testDataPersistence.persistProduct();
		Product result = repository.get(expected.getId());
		assertThat(result, is(expected));
	}

	@Test
	public void testImportData_emptyCategory() throws Exception {
		ImportRequest request = creator.createImportRequest();

		Set<Product> results = repository.importData(request);

		List<Product> expected = testDataPersistence.getAllProducts();
        validator.fullValidate(new ArrayList<Product>(results));
		assertThat(new ArrayList<Product>(results), is(expected));
	}

    @Test
    public void testImportData_existCategory() throws Exception {
        ImportRequest request = creator.createImportRequest();
        Category persistenceCategory = testDataPersistence.persistCategory();
        request.setCategoryName(persistenceCategory.getName());

        Set<Product> results = repository.importData(request);

        List<Product> expected = testDataPersistence.getAllProducts();
        validator.fullValidate(new ArrayList<Product>(results));
        assertThat(new ArrayList<Product>(results), is(expected));
    }

    @Test
    public void testGetAll() throws Exception {
        List<Product> expected = testDataPersistence.persistProducts(200);

        List<Product> result = repository.getAll();

        validator.fullValidate(result);
        assertThat(result, is(expected));
    }

	@Test
	public void testGetByCategoryId() throws Exception {
		Product product = testDataPersistence.persistProduct();
		List<Product> result = repository.getByCategoryId(product.getId());
		assertThat(result.get(0), is(product));
	}

	@Test
	public void testGetByCategory() throws Exception {
		List<Product> products = testDataPersistence.persistProducts(200);
		Product product = products.get(0);
		List<Product> result = repository.getByCategory(product.getCategory());
		assertThat(result.get(0), is(product));
	}
}
package ru.galeev.aggrecalor.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.test.TestDataCreator;
import ru.galeev.aggrecalor.test.TestDataPersistence;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(locations = {"classpath:test-spring-config.xml"})
public class CategoryRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private CategoryRepository repository;

    @Autowired
    private TestDataCreator creator;

    @Autowired
    private TestDataPersistence testDataPersistence;

    @Test
    public void testCreateByCategory() throws Exception {
        Category result = repository.create(creator.createRandomCategory());
        assertThat(result, is(testDataPersistence.getAllCategories().get(0)));
    }

    @Test
    public void testCreateByName() throws Exception {
        Category result = repository.create(creator.generateString());
        assertThat(result, is(testDataPersistence.getAllCategories().get(0)));
    }

    @Test
    public void testUpdate() throws Exception {
        Category category = testDataPersistence.persistCategory();
        category.setName("TEST");
        Category result = repository.update(category);
        assertThat(result, is(category));
    }

    @Test
    public void testDelete() throws Exception {
        Category category = testDataPersistence.persistCategory();
        repository.delete(category);
        assertThat(0, is(testDataPersistence.getAllCategories().size()));
    }

	@Test
	public void testDelete_byId() throws Exception {
		Category category = testDataPersistence.persistCategory();
		repository.delete(category.getId());
		assertThat(0, is(testDataPersistence.getAllCategories().size()));
	}

    @Test
    public void testGetAll() throws Exception {
        List<Category> expected = testDataPersistence.persistCategories(200);
        List<Category> result = repository.getAll();
        assertThat(result, is(expected));
    }

    @Test
    public void testGetByName() throws Exception {
        Category expected = testDataPersistence.persistCategory();
        Category result = repository.get(expected.getName());
        assertThat(result, is(expected));
    }

	@Test
	public void testGetById() throws Exception {
		Category expected = testDataPersistence.persistCategory();
		Category result = repository.get(expected.getId());
		assertThat(result, is(expected));
	}
}
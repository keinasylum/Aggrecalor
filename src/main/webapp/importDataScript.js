//health-diet.ru
(function parsePageToJson() {
    var data = jQuery(".content tbody tr td");
    var name = null, kilocalories = 0, proteins = 0, fats = 0, carbohydrates = 0
    var result = [];
    for (var i in data) {
        if ((i % 5) == 0) {
            name = data[i].outerText;
        }
        if ((i % 5) == 1) {
            kilocalories = data[i].outerText;
        }
        if ((i % 5) == 2) {
            proteins = data[i].outerText;
        }
        if ((i % 5) == 3) {
            fats = data[i].outerText;
        }
        if ((i % 5) == 4) {
            carbohydrates = data[i].outerText;
        }
        if (name != null && kilocalories != 0 && proteins != 0 && fats != 0 && carbohydrates != 0) {
            result.push({
                name: name,
                kilocalories: parseFloat(kilocalories.replace(",", ".")),
                proteins: parseFloat(proteins.replace(",", ".")),
                fats: parseFloat(fats.replace(",", ".")),
                carbohydrates: parseFloat(carbohydrates.replace(",", "."))
            });
            name = null; kilocalories = 0; proteins = 0; fats = 0; carbohydrates = 0;
        }
    }
    jQuery.ajax({
        url: "http://localhost:8081/aggrecalor/product/import",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({
            products: result, categoryName: jQuery("#mainColumn_content h3").text().replace("Таблица калорийности продуктов питания.", "").trim().replace(".", "")
        }),
        contentType: "application/json",
        success: function(data) {
            return data;
        }
    })
})();

var aggrecalor = angular.module('aggrecalor', [
    'ngTable',
    'ui.bootstrap',
    'ngRoute',
	'productService',
	'categoryService',
	'calculationService'
], function($routeProvider, $locationProvider) {
    $routeProvider.when('/aggrecalor/product', {templateUrl: 'aggrecalor/views/product.jsp', controller: 'ProductController'});
	$routeProvider.when('/aggrecalor/calculator', {templateUrl: 'aggrecalor/views/calculator.jsp', controller: 'CalculatorController'});
    $routeProvider.otherwise({redirectTo: '/aggrecalor/product'});
    $locationProvider.html5Mode(true);
});
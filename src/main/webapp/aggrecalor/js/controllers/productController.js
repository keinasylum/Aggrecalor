aggrecalor.controller('ProductController', function($scope, $filter, $modal, ngTableParams, Product, Category) {
	$scope.products = null;
	$scope.categories = null;
	$scope.appliedCategory = null;
	$scope.showProductSettings = false;
	$scope.showCategorySettings = false;

	$scope.productTable = new ngTableParams({
		page: 1,
		count: 25
	}, {
		groupBy: function(item) {
			return item.category.name;
		},
		getData: function($defer, params) {
			if ($scope.products) {
				var filteredData = params.filter() ? $filter('filter')($scope.products, params.filter()) : $scope.products;
				var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : $scope.products;
				params.total(orderedData.length);
				$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
			} else {
                updateProductTableFromServer();
				updateCategoriesFromServer();
			}
		}
	});

	$scope.selectCategory = function(category) {
		if (!$scope.showCategorySettings) {
			if (($scope.appliedCategory) && ($scope.appliedCategory.$$active) && ($scope.appliedCategory.id == category.id)) {
				Product.all(function(data) {
					$scope.products = data;
					$scope.appliedCategory = category;
					$scope.productTable.reload();
					updateStatusForCategories(category, true);
				});
			} else {
				Product.byCategoryId({categoryId : category.id}, function(data) {
					$scope.products = data;
					$scope.appliedCategory = category;
					$scope.productTable.reload();
					updateStatusForCategories(category, false);
				});
			}
		}
	};

	$scope.toggleShowProductSettings = function() {
		if ($scope.showProductSettings) {
			$scope.showProductSettings = false;
		} else {
			$scope.showProductSettings = true;
		}
	};

	$scope.toggleShowCategorySettings = function() {
		if ($scope.showCategorySettings) {
			$scope.showCategorySettings = false;
		} else {
			$scope.showCategorySettings = true;
		}
	};

	$scope.isCategoryUsed = function(category) {
		return false;
	};

	$scope.clearAllFiltersAndSorting = function() {
		$scope.productTable.sorting({});
		$scope.productTable.filter({})
	};

    $scope.openUpdateCategory = function(updated) {
        var modalInstance = $modal.open({
            templateUrl: 'updateCategory',
            controller: 'UpdateCategoryController',
			backdrop: 'static',
            resolve: {
                items: function () {
                    return {
                        category: Category,
                        updated: updated
                    }
                }
            }
        });
        modalInstance.result.then(function() {
            updateProductTableFromServer();
        });
    };

	$scope.openCreateCategory = function() {
		var modalInstance = $modal.open({
			templateUrl: 'createCategory',
			controller: 'CreateCategoryController',
			backdrop: 'static',
			resolve: {
				items: function () {
					return {
						category: Category
					}
				}
			}
		});
		modalInstance.result.then(function() {
			updateCategoriesFromServer();
		});
	};

	$scope.openDeleteCategory = function(deletable) {
		var modalInstance = $modal.open({
			templateUrl: 'deleteCategory',
			controller: 'DeleteCategoryController',
			backdrop: 'static',
			resolve: {
				items: function () {
					return {
						category: Category,
						deletable: deletable
					}
				}
			}
		});
		modalInstance.result.then(function() {
			updateCategoriesFromServer();
		});
	};

	$scope.openCreateProduct = function() {
		var modalInstance = $modal.open({
			templateUrl: 'createProduct',
			controller: 'CreateProductController',
			backdrop: 'static',
			resolve: {
				items: function () {
					return {
						category: Category,
						product: Product
					}
				}
			}
		});
		modalInstance.result.then(function() {
			updateProductTableFromServer();
		});
	};

	$scope.openUpdateProduct = function(updated) {
		var modalInstance = $modal.open({
			templateUrl: 'updateProduct',
			controller: 'UpdateProductController',
			backdrop: 'static',
			resolve: {
				items: function () {
					return {
						category: Category,
						product: Product,
						updated: updated
					}
				}
			}
		});
		modalInstance.result.then(function() {
			updateProductTableFromServer();
		});
	};

	$scope.openDeleteProduct = function(deletable) {
		var modalInstance = $modal.open({
			templateUrl: 'deleteProduct',
			controller: 'DeleteProductController',
			backdrop: 'static',
			resolve: {
				items: function () {
					return {
						product: Product,
						deletable: deletable
					}
				}
			}
		});
		modalInstance.result.then(function() {
			updateProductTableFromServer();
		});
	}

    var updateProductTableFromServer = function() {
        Product.all(function(data) {
            $scope.products = data;
            $scope.productTable.reload();
        });
    };

	var updateCategoriesFromServer = function() {
		Category.all(function(data) {
			$scope.categories = data;
		});
	};

    var updateStatusForCategories = function(activeCategory, disableAll) {
        angular.forEach($scope.categories, function (value) {
            if (value.$$active) {
                value.$$active = false;
            }
        });
        if (!disableAll) {
            activeCategory.$$active = true;
        }
    };
});
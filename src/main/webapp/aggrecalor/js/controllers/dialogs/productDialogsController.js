aggrecalor.controller('CreateProductController', function($scope, $modalInstance, items) {
	$scope.Category = items.category;
	$scope.Product = items.product;
    $scope.ProductMass = {
        mass: null
    };
	$scope.ProductRequest = {
		name: null,
		proteins: null,
		fats: null,
		carbohydrates: null,
		kilocalories: null,
		category: null
	}
	$scope.Category.all(function(data) {
		$scope.categories = data;
	});
	$scope.save = function() {
        $scope.adjust();
		$scope.Product.save($scope.ProductRequest, function(data) {
			$modalInstance.close();
		});
	}
	$scope.cancel = function() {
		$modalInstance.close();
	}
    $scope.adjust = function() {
        for (var n in $scope.ProductRequest) {
            $scope.ProductRequest[n] = adjustMass($scope.ProductRequest[n], $scope.ProductMass.mass);
        }
        $scope.ProductMass.mass = 100;
    };
    var adjustMass = function(startingMass, startingGrams) {
        if (angular.isNumber(startingMass) && angular.isNumber(startingGrams)) {
            var result = startingMass * 100 / startingGrams;
            return parseFloat(result.toFixed(2));
        } else {
            return startingMass;
        }
    };
});

aggrecalor.controller('UpdateProductController', function($scope, $modalInstance, items) {
	$scope.Category = items.category;
	$scope.Product = items.product;
	$scope.updated = items.updated;
	$scope.Category.all(function(data) {
		$scope.categories = data;
	});
	$scope.update = function() {
		$scope.Product.update($scope.updated, function(data) {
			$modalInstance.close();
		});
	}
	$scope.cancel = function() {
		$modalInstance.close();
	}
});

aggrecalor.controller('DeleteProductController', function($scope, $modalInstance, items) {
	$scope.Product = items.product;
	$scope.deletable = items.deletable;
	$scope.remove = function() {
		$scope.Product.remove({serviceMethod: $scope.deletable.id}, function(data) {
			$modalInstance.close();
		});
	}
	$scope.cancel = function() {
		$modalInstance.close();
	}
});

var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
aggrecalor.directive('floatValidate', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {
				if (FLOAT_REGEXP.test(viewValue) && viewValue > 0) {
                    ctrl.$setValidity('float', true);
                    return parseFloat(viewValue.replace(',', '.'));
				} else {
					ctrl.$setValidity('float', false);
					return undefined;
				}
			});
		}
	};
});
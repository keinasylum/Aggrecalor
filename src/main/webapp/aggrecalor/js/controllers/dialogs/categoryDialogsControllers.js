aggrecalor.controller('CreateCategoryController', function($scope, $modalInstance, items) {
	$scope.Category = items.category;
	$scope.CategoryRequest = {
		id: null,
		name: null
	}
	$scope.save = function() {
		$scope.Category.save($scope.CategoryRequest, function(data) {
			$modalInstance.close();
		});
	}
	$scope.cancel = function() {
		$modalInstance.close();
	}
});

aggrecalor.controller('UpdateCategoryController', function($scope, $modalInstance, items) {
    $scope.Category = items.category;
    $scope.updated = items.updated;
    $scope.update = function() {
        $scope.Category.update($scope.updated, function(data) {
            $modalInstance.close();
        }, function(error) {
            $scope.Category.byId({id: $scope.updated.id}, function(data) {
                $scope.updated.name = data.name;
            });
        });
    }
	$scope.cancel = function() {
		$modalInstance.close();
	}
});

aggrecalor.controller('DeleteCategoryController', function($scope, $modalInstance, items) {
	$scope.Category = items.category;
	$scope.deletable = items.deletable;
	$scope.remove = function() {
		$scope.Category.remove({serviceMethod: $scope.deletable.id}, function(data) {
			$modalInstance.close();
		});
	}
	$scope.cancel = function() {
		$modalInstance.close();
	}
});
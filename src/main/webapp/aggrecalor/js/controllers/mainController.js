aggrecalor.controller('MainController', function ($scope, $location) {
	$scope.toggleTheme = function () {
		var bootstrap = "bootstrap.css";
		var bootstrapSlate = "bootstrap-slate.css";
		var bootstrapTheme = "bootstrap-theme.css";
		var stump = "bootstrap-stump.css";
		var links = jQuery('link');
		var activeScheme = null;
		angular.forEach(links, function (item) {
			if (contains(item.href, bootstrap)) {
				item.href = item.href.replace(bootstrap, bootstrapSlate);
				activeScheme = bootstrap;
			} else if (contains(item.href, bootstrapSlate)) {
				item.href = item.href.replace(bootstrapSlate, bootstrap);
				activeScheme = bootstrapSlate;
			} else if (contains(item.href, bootstrapTheme) || contains(item.href, stump)) {
				if (activeScheme == bootstrap) {
					item.href = item.href.replace(bootstrapTheme, stump);
				} else {
					item.href = item.href.replace(stump, bootstrapTheme);
				}
			}
		});
	};

	$scope.isActive = function(viewLocation) {
		return viewLocation == $location.path();
	};
});

var contains = function (text, textPart, ignoreCase) {
	if (!text) {
		return false;
	}
	if (!textPart) {
		return true;
	}
	if (ignoreCase) {
		return (text.toUpperCase().indexOf(textPart.toUpperCase()) >= 0);
	} else {
		return (text.indexOf(textPart) >= 0);
	}
};
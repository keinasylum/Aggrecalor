aggrecalor.controller('CalculatorController', function ($scope, $route, Product, Calculation) {
	Product.all(function (data) {
		$scope.products = data;
	});
	Calculation.all(function (data) {
		angular.forEach(data, function(calculation) {
			$scope.calculate(calculation);
		});
		$scope.calculations = data;
	});

	$scope.addCalculatedProduct = function (calculation) {
		calculation.calculatedProducts.push({
			product: undefined,
			mass: undefined
		});
	};

	$scope.removeCalculatedProduct = function (calculation, calculatedProduct) {
		var index = calculation.calculatedProducts.indexOf(calculatedProduct);
		if (index > -1) {
			calculation.calculatedProducts.splice(index, 1);
		}
		$scope.calculate(calculation);
	};

	$scope.addCalculation = function () {
		$scope.calculations.push({
			name: "",
			calculatedProducts: [{
				product: undefined,
				mass: undefined
			}]
		});
	};

	$scope.removeCalculation = function (calculation) {
		var index = $scope.calculations.indexOf(calculation);
		if (index > -1) {
			$scope.calculations.splice(index, 1);
		}
	};

	$scope.calculate = function (calculation) {
		calculation.$$kilocalories = 0, calculation.$$proteins = 0, calculation.$$fats = 0, calculation.$$carbohydrates = 0, calculation.$$mass = 0;
		angular.forEach(calculation.calculatedProducts, function (calculatedProduct) {
			var mass = parseFloat(calculatedProduct.mass);
			var product = calculatedProduct.product;
			var calcParameter = function (param) {
				return param / 100 * mass;
			};
			var fixParameter = function (param) {
				return parseFloat(param.toFixed(2));
			};
			var float_regexp = /^\-?\d+((\.|\,)\d+)?$/;
			if (!angular.isUndefined(product) && !angular.isUndefined(calculatedProduct.mass) && float_regexp.test(calculatedProduct.mass)) {
				calculatedProduct.$$kilocalories = fixParameter(calcParameter(product.kilocalories));
				calculatedProduct.$$proteins = fixParameter(calcParameter(product.proteins));
				calculatedProduct.$$fats = fixParameter(calcParameter(product.fats));
				calculatedProduct.$$carbohydrates = calcParameter(product.carbohydrates);
				calculation.$$kilocalories = fixParameter(calculation.$$kilocalories + calcParameter(product.kilocalories));
				calculation.$$proteins = fixParameter(calculation.$$proteins + calcParameter(product.proteins));
				calculation.$$fats = fixParameter(calculation.$$fats + calcParameter(product.fats));
				calculation.$$carbohydrates = fixParameter(calculation.$$carbohydrates + calcParameter(product.carbohydrates));
				calculation.$$mass += mass;
			}
		});
	};

	$scope.save = function(calculation) {
		Calculation.save(calculation, function(data) {
			$route.reload();
		});
	};

	$scope.removeCalculation = function(calculation) {
		Calculation.deleteCalculation({id: calculation.id}, function(data) {
			$route.reload();
		});
	};

	$scope.removeCalculationProduct = function(calculationProduct) {
		Calculation.deleteCalculatedProduct({id: calculationProduct.id}, function(data) {
			$route.reload();
		});
	};
});
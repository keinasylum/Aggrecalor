var baseUrl = 'http://localhost:8081/aggrecalor';

angular.module('productService', ['ngResource']).factory('Product', function ($resource) {
	return $resource(baseUrl + '/product/:serviceMethod?categoryId=:categoryId', {}, {
		all: {method: 'GET', params: {serviceMethod: 'all'}, isArray: true},
		byCategoryId: {method: 'GET', params: {serviceMethod: 'byCategoryId', categoryId: ':categoryId'}, isArray: true},
		update: {method: 'PUT'}
	});
});

angular.module('categoryService', ['ngResource']).factory('Category', function ($resource) {
	return $resource(baseUrl + '/category/:serviceMethod?id=:id&name=:name', {}, {
		all: {method: 'GET', params: {serviceMethod: 'all'}, isArray: true},
		byId: {method: 'GET', params: {serviceMethod: 'byId', id: ':id'}},
		byName: {method: 'GET', params: {serviceMethod: 'byName', name: ':name'}},
		update: {method: 'PUT'}
	});
});

angular.module('calculationService', ['ngResource']).factory('Calculation', function ($resource) {
	return $resource(baseUrl + '/calculation/:pathOrMethod/:id',
		{
			id: "@id"
		}, {
			all: {method: 'GET', params: {pathOrMethod: 'all'}, isArray: true},
			deleteCalculation: {method: 'DELETE'},
			deleteCalculatedProduct: {method: 'DELETE', params: {pathOrMethod: 'calculatedProduct'}}
		});
});
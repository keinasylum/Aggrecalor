<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div ng-controller="CalculatorController">
	<div class="container">
		<accordion ng-model="oneAtATime" ng-repeat="calculation in calculations">
			<div class="row">
				<div class="col-md-11">
					<accordion-group is-open="true" heading="{{calculation.name}}" is-open="true">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" ng-model="calculation.name" placeholder="Наименование">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<button class="btn btn-default btn-block" ng-click="save(calculation)">Сохранить</button>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-1">
								<b>Белки</b>
							</div>
							<div class="col-md-1">
								<b>Жиры</b>
							</div>
							<div class="col-md-1">
								<b>Углеводы</b>
							</div>
							<div class="col-md-1">
								<b>Калории</b>
							</div>
							<div class="col-md-1">
								<b>Масса</b>
							</div>
							<div class="col-md-1"></div>
						</div>
						<div class="row" ng-repeat="calculatedProduct in calculation.calculatedProducts">
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control" ng-model="calculatedProduct.product" placeholder="Продукт" typeahead="product as product.name for product in products | filter:{name:$viewValue} | limitTo:8" ng-change="calculate(calculation)">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<input type="text" class="form-control" ng-model="calculatedProduct.mass" placeholder="Грамм" ng-change="calculate(calculation)">
								</div>
							</div>
							<div class="col-md-1">
								<b>{{calculatedProduct.$$proteins}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculatedProduct.$$fats}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculatedProduct.$$carbohydrates}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculatedProduct.$$kilocalories}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculatedProduct.mass}}</b>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<button class="btn btn-default" ng-click="removeCalculationProduct(calculation, calculatedProduct)">
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<button class="btn btn-default btn-block" ng-click="addCalculatedProduct(calculation)">Добавить продукт</button>
								</div>
							</div>
							<div class="col-md-1">
								<b>{{calculation.$$proteins}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculation.$$fats}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculation.$$carbohydrates}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculation.$$kilocalories}}</b>
							</div>
							<div class="col-md-1">
								<b>{{calculation.$$mass}}</b>
							</div>
							<div class="col-md-1"></div>
						</div>
					</accordion-group>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<button class="btn btn-default">
							<span class="glyphicon glyphicon-remove" ng-click="removeCalculation(calculation)"></span>
						</button>
					</div>
				</div>
			</div>
		</accordion>
		<div class="row">
			<div class="col-md-11">
				<button class="btn btn-default btn-block" ng-click="addCalculation()">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</div>
		</div>
	</div>
</div>
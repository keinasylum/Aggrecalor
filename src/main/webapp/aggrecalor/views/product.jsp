<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div ng-controller="ProductController">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-6">
							<h3 class="panel-title">Категории</h3>
						</div>
						<div class="col-md-6">
							<button class="btn pull-right btn-default btn-sm" ng-click="toggleShowCategorySettings()" ng-class="{'true':'btn pull-right btn-default btn-sm active', 'false':'btn pull-right btn-default btn-sm'}[showCategorySettings]">
								<span class="glyphicon glyphicon-cog"></span>
							</button>
						</div>
					</div>
				</div>
				<button class="btn btn-default btn-block" ng-show="showCategorySettings" ng-click="openCreateCategory()">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
				<div class="panel-body">
					<div class="list-group">
						<a href="#" class="list-group-item" ng-repeat="category in categories" ng-click="selectCategory(category)" ng-class="{'true':'list-group-item active', 'false':'list-group-item'}[category.$$active && !showCategorySettings]">
							<div class="row">
								<div class="col-md-12" ng-class="{'false':'col-md-12', 'true':'col-md-7'}[showCategorySettings]">
									{{category.name}}
								</div>
								<div class="col-md-5" ng-show="showCategorySettings">
									<div class="btn-group">
										<button class="btn btn-default btn-sm" ng-click="openUpdateCategory(category)">
											<span class="glyphicon glyphicon-pencil"></span>
										</button>
										<button class="btn btn-default btn-sm" ng-class="{'true':'btn btn-default btn-sm disabled', 'false':'btn btn-default btn-sm'}[isCategoryUsed(category)]" ng-click="openDeleteCategory(category)">
											<span class="glyphicon glyphicon-remove"></span>
										</button>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-6">
							<h3 class="panel-title">Продукты</h3>
						</div>
						<div class="col-md-6">
							<button class="btn pull-right btn-default btn-sm" ng-click="toggleShowProductSettings()" ng-class="{'true':'btn pull-right btn-default btn-sm active', 'false':'btn pull-right btn-default btn-sm'}[showProductSettings]">
								<span class="glyphicon glyphicon-cog"></span>
							</button>
						</div>
					</div>
				</div>
				<button class="btn btn-default btn-block" ng-show="showProductSettings" ng-click="openCreateProduct()">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
				<div class="panel-body">
					<table ng-table="productTable" class="table table table-condensed table-hover" show-filter="true">
						<tbody ng-repeat="group in $groups">
						<tr class="ng-table-group">
							<td colspan="{{$columns.length}}">
								<strong>{{group.value}}</strong>
							</td>
						</tr>
						<tr ng-repeat="product in group.data">
							<td data-title="'Наименование'" filter="{ 'name': 'name' }"  sortable="'name'" class="col-md-3">
								<em>{{product.name}}</em>
							</td>
							<td data-title="'Колорийность'" sortable="'kilocalories'" class="col-md-1">
								<em>{{product.kilocalories | number}}</em>
							</td>
							<td data-title="'Белки'" sortable="'proteins'" class="col-md-1">
								<em>{{product.proteins | number}}</em>
							</td>
							<td data-title="'Жиры'" sortable="'fats'" class="col-md-1">
								<em>{{product.fats | number}}</em>
							</td>
							<td data-title="'Углеводы'" sortable="'carbohydrates'" class="col-md-1">
								<em>{{product.carbohydrates | number}}</em>
							</td>
							<td ng-show="showProductSettings" class="col-md-1">
								<div class="btn-group">
									<button class="btn btn-default btn-sm" ng-click="openUpdateProduct(product)">
										<span class="glyphicon glyphicon-pencil"></span>
									</button>
									<button class="btn btn-default btn-sm" ng-click="openDeleteProduct(product)">
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
					<script type="text/ng-template" id="ng-table/filters/name.html">
						<div class="row">
							<div class="col-md-10">
								<input type="text" class="form-control input-sm" ng-model="params.filter()[name]" name="filter-name" placeholder="поиск..." />
							</div>
							<div class="col-md-1">
								<button class="btn btn-default" ng-click="clearAllFiltersAndSorting()" tooltip="Сбросить фильтры и сортировки" tooltip-trigger="mouseenter" tooltip-placement="right">
									<span class="glyphicon glyphicon-remove"></span>
								</button>
							</div>
						</div>
					</script>
				</div>
			</div>
		</div>
	</div>
    <div ng-include="'views/dialogs/categoryDialogs.html'"></div>
	<div ng-include="'views/dialogs/productDialogs.html'"></div>
</div>
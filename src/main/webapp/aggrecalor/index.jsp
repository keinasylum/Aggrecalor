<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="<c:url value="lib/jquery/jquery-2.1.1.js" />"></script>
    <script type="text/javascript" src="<c:url value="lib/angular/angular.js" />"></script>
    <script type="text/javascript" src="<c:url value="lib/angular/angular-resource.js" />"></script>
    <script type="text/javascript" src="<c:url value="lib/angular/angular-route.js" />"></script>
	<script type="text/javascript" src="<c:url value="lib/angular/ng-table.js" />"></script>
    <script type="text/javascript" src="<c:url value="lib/angular/ui-bootstrap-tpls-0.11.2.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="lib/bootstrap/bootstrap.js" />"></script>
    <script type="text/javascript" src="<c:url value="js/app.js" />"></script>
	<script type="text/javascript" src="<c:url value="js/services.js" />"></script>
    <script type="text/javascript" src="<c:url value="js/controllers/productController.js" />"></script>
	<script type="text/javascript" src="<c:url value="js/controllers/mainController.js" />"></script>
	<script type="text/javascript" src="<c:url value="js/controllers/calculatorController.js" />"></script>
    <script type="text/javascript" src="<c:url value="js/controllers/dialogs/categoryDialogsControllers.js" />"></script>
	<script type="text/javascript" src="<c:url value="js/controllers/dialogs/productDialogsController.js" />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="css/angular/ng-table.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="css/bootstrap/bootstrap.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="css/bootstrap/bootstrap-theme.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="css/bootstrap/bootstrap-stump.css" />">
    <style>
		body { padding-top: 60px; }
	</style>
</head>
<body ng-app="aggrecalor" ng-controller="MainController">
<div class="container-fluid">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="navbar-header">
						<a class="navbar-brand" href="product">Aggrecalor</a>
					</div>
					<ul class="nav navbar-nav">
						<li ng-class="{ active: isActive('/aggrecalor/product')}"><a href="product">Продукты</a></li>
						<li ng-class="{ active: isActive('/aggrecalor/calculator')}"><a href="calculator">Калькулятор</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<button type="button" class="btn btn-default navbar-btn" ng-click="toggleTheme()">
							<span class="glyphicon glyphicon-refresh"></span>
						</button>
					</ul>
				</div>
			</div>
        </div>
    </nav>
    <div ng-view class="view-frame"></div>
</div>
</body>
</html>
package ru.galeev.aggrecalor.entities.validators;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.entities.Product;

import java.util.List;

@Service
public class EntityValidator {

    private static final String CATEGORY = "Category";
	private static final String PRODUCT = "Product";
    private static final String PRODUCTS = "Products";

	private static final String CAN_NOT_BE_NULL = " can not be null";
	private static final String CAN_NOT_BE_NEGATIVE = " can not be negative";
    private static final String CAN_NOT_BE_EMPTY = " can not be empty";

	private static final String CATEGORY_ID = "Category.id";
	private static final String CATEGORY_NAME = "Category.name";
	private static final String PRODUCT_NAME = "Product.name";
	private static final String PRODUCT_CARBOHYDRATES = "Product.carbohydrates";
	private static final String PRODUCT_FATS = "Product.fats";
	private static final String PRODUCT_KILOCALORIES = "Product.kilocalories";
	private static final String PRODUCT_PROTEINS = "Product.proteins";
	private static final String PRODUCT_ID = "Product.id";

    public void fullValidate(Category category) {
		prePersistValidate(category);
		Assert.notNull(category.getId(), CATEGORY_ID + CAN_NOT_BE_NULL);
	}

	public void prePersistValidate(Category category) {
		Assert.notNull(category, CATEGORY + CAN_NOT_BE_NULL);
		Assert.notNull(category.getName(), CATEGORY_NAME + CAN_NOT_BE_NULL);
        Assert.isTrue(category.getName().trim().length() != 0, CATEGORY_NAME + CAN_NOT_BE_EMPTY);
	}

	public void fullValidate(Product product) {
		prePersistValidate(product);
		Assert.notNull(product.getId(), PRODUCT_ID + CAN_NOT_BE_NULL);
        fullValidate(product.getCategory());
	}

    public void fullValidate(List<Product> products) {
        Assert.notNull(products, PRODUCTS + CAN_NOT_BE_NULL);
        for (Product product : products) {
            fullValidate(product);
        }
    }

	public void prePersistValidate(Product product) {
		Assert.notNull(product, PRODUCT + CAN_NOT_BE_NULL);
		Assert.notNull(product.getName(), PRODUCT_NAME + CAN_NOT_BE_NULL);
        Assert.isTrue(product.getName().trim().length() != 0, PRODUCT_NAME + CAN_NOT_BE_EMPTY);
		Assert.notNull(product.getCarbohydrates(), PRODUCT_CARBOHYDRATES + CAN_NOT_BE_NULL);
		Assert.notNull(product.getFats(), PRODUCT_FATS + CAN_NOT_BE_NULL);
		Assert.notNull(product.getKilocalories(), PRODUCT_KILOCALORIES + CAN_NOT_BE_NULL);
		Assert.notNull(product.getProteins(), PRODUCT_PROTEINS + CAN_NOT_BE_NULL);
		Assert.isTrue(product.getCarbohydrates() > 0, PRODUCT_CARBOHYDRATES + CAN_NOT_BE_NEGATIVE);
		Assert.isTrue(product.getFats() > 0, PRODUCT_FATS + CAN_NOT_BE_NEGATIVE);
		Assert.isTrue(product.getKilocalories() > 0, PRODUCT_KILOCALORIES + CAN_NOT_BE_NEGATIVE);
		Assert.isTrue(product.getProteins() > 0, PRODUCT_PROTEINS + CAN_NOT_BE_NEGATIVE);
	}
}

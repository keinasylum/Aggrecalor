package ru.galeev.aggrecalor.entities.requests;

import ru.galeev.aggrecalor.entities.Product;

import java.util.Set;

public class ImportRequest {

	private String categoryName;

    private Set<Product> products;

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
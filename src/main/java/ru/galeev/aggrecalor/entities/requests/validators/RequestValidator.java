package ru.galeev.aggrecalor.entities.requests.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.galeev.aggrecalor.entities.Product;
import ru.galeev.aggrecalor.entities.requests.ImportRequest;
import org.springframework.util.Assert;
import ru.galeev.aggrecalor.entities.validators.EntityValidator;

import java.util.Set;

@Service
public class RequestValidator {

    @Autowired
	private EntityValidator validator;

	private static final String CAN_NOT_BE_NULL = " can not be null";
    private static final String CAN_NOT_BE_EMPTY = " can not be empty";
	private static final String IMPORT_REQUEST = "ImportRequest";
	private static final String IMPORT_REQUEST_CATEGORY_NAME = "ImportRequest.categoryName";
	private static final String IMPORT_REQUEST_PRODUCTS = "ImportRequest.products";
	private static final String FIELD = "Field";
	private static final String ID = "ID";

	public void validate(ImportRequest request) {
		Assert.notNull(request, IMPORT_REQUEST + CAN_NOT_BE_NULL);
		Assert.notNull(request.getCategoryName(), IMPORT_REQUEST_CATEGORY_NAME + CAN_NOT_BE_NULL);
        Assert.isTrue(request.getCategoryName().trim().length() != 0, IMPORT_REQUEST_CATEGORY_NAME + CAN_NOT_BE_EMPTY);
		Assert.notNull(request.getProducts(), IMPORT_REQUEST_PRODUCTS + CAN_NOT_BE_NULL);
		Set<Product> products = request.getProducts();
		for (Product product : products) {
			validator.prePersistValidate(product);
		}
	}

	public void validate(String field) {
		Assert.notNull(field, FIELD + CAN_NOT_BE_NULL);
	}

	public void validate(Long id) {
		Assert.notNull(id, ID + CAN_NOT_BE_NULL);
	}
}

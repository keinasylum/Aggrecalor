package ru.galeev.aggrecalor.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;

@Entity
@NamedQueries({
		@NamedQuery(name = "Category.getByName", query = "select c from Category c where c.name = :name"),
		@NamedQuery(name = "Category.getById", query = "select c from Category c where c.id = :id"),
        @NamedQuery(name = "Category.getAll", query = "select c from Category c")
})
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 200, unique = true)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if(o.getClass() != this.getClass()) {
			return false;
		}
		Category that = (Category)o;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(getId(), that.getId());
		builder.append(getName(), that.getName());
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(getId());
		builder.append(getName());
		return builder.hashCode();
	}
}

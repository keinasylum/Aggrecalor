package ru.galeev.aggrecalor.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;

@Entity
@NamedQueries({
		@NamedQuery(name = "CalculatedProduct.getById", query = "select c from CalculatedProduct c where c.id = :id")
})
public class CalculatedProduct {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	private Product product;

	@Column
	private Double mass;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getMass() {
		return mass;
	}

	public void setMass(Double mass) {
		this.mass = mass;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if(o.getClass() != this.getClass()) {
			return false;
		}
		CalculatedProduct that = (CalculatedProduct) o;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(getId(), that.getId());
		builder.append(getProduct(), that.getProduct());
		builder.append(getMass(), that.getMass());
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(getId());
		builder.append(getProduct());
		builder.append(getMass());
		return builder.hashCode();
	}
}
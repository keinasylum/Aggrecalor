package ru.galeev.aggrecalor.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries({
		@NamedQuery(name = "Calculation.getAll", query = "select c from Calculation c"),
		@NamedQuery(name = "Calculation.getById", query = "select c from Calculation c where c.id = :id")
})
public class Calculation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String name;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<CalculatedProduct> calculatedProducts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CalculatedProduct> getCalculatedProducts() {
		return calculatedProducts;
	}

	public void setCalculatedProducts(List<CalculatedProduct> calculatedProducts) {
		this.calculatedProducts = calculatedProducts;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if(o.getClass() != this.getClass()) {
			return false;
		}
		Calculation that = (Calculation) o;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(getId(), that.getId());
		builder.append(getCalculatedProducts(), that.getCalculatedProducts());
		builder.append(getName(), that.getName());
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(getId());
		builder.append(getCalculatedProducts());
		builder.append(getName());
		return builder.hashCode();
	}
}

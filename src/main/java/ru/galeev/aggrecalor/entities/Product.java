package ru.galeev.aggrecalor.entities;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import ru.galeev.aggrecalor.serialization.JsonDateSerializer;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "Product.getAll", query = "select c from Product c"),
		@NamedQuery(name = "Product.getByCategory", query = "select c from Product c where c.category = :category"),
		@NamedQuery(name = "Product.getById", query = "select c from Product c where c.id = :id")
})
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 200, unique = true)
    private String name;

    @Column(nullable = false)
    private Double proteins;

    @Column(nullable = false)
    private Double kilocalories;

    @Column(nullable = false)
    private Double fats;

    @Column(nullable = false)
    private Double carbohydrates;

	@Column(nullable = false, updatable = false)
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date creationDate;

	@Column(nullable = false, updatable = false)
	private Boolean imported;

	@ManyToOne
	private Category category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getProteins() {
        return proteins;
    }

    public void setProteins(Double proteins) {
        this.proteins = proteins;
    }

    public Double getKilocalories() {
        return kilocalories;
    }

    public void setKilocalories(Double kilocalories) {
        this.kilocalories = kilocalories;
    }

    public Double getFats() {
        return fats;
    }

    public void setFats(Double fats) {
        this.fats = fats;
    }

    public Double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(Double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Boolean getImported() {
		return imported;
	}

	public void setImported(Boolean imported) {
		this.imported = imported;
	}

	@Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if(o.getClass() != this.getClass()) {
            return false;
        }
        Product that = (Product) o;
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(getCarbohydrates(), that.getCarbohydrates());
        builder.append(getFats(), that.getFats());
        builder.append(getId(), that.getId());
        builder.append(getKilocalories(), that.getKilocalories());
        builder.append(getProteins(), that.getProteins());
        builder.append(getName(), that.getName());
		builder.append(getCategory(), that.getCategory());
		builder.append(getCreationDate(), that.getCreationDate());
		builder.append(getImported(), that.getImported());
        return builder.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getCarbohydrates());
        builder.append(getFats());
        builder.append(getId());
        builder.append(getKilocalories());
        builder.append(getProteins());
        builder.append(getName());
		builder.append(getCategory());
		builder.append(getCreationDate());
		builder.append(getImported());
        return builder.hashCode();
    }
}
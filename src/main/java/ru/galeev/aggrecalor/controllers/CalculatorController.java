package ru.galeev.aggrecalor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.galeev.aggrecalor.entities.CalculatedProduct;
import ru.galeev.aggrecalor.entities.Calculation;
import ru.galeev.aggrecalor.repository.CalculatedProductRepository;
import ru.galeev.aggrecalor.repository.CalculationRepository;

import java.util.List;

@RestController
@RequestMapping("calculation")
public class CalculatorController {

	@Autowired
	private CalculationRepository calculationRepository;

	@Autowired
	private CalculatedProductRepository calculatedProductRepository;

	@RequestMapping(value = "all", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	List<Calculation> getAll() {
		return calculationRepository.getAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	Calculation create(@RequestBody Calculation calculation) {
		return calculationRepository.create(calculation);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseBody
	Calculation deleteCalculation(@PathVariable Long id) {
		return calculationRepository.delete(id);
	}

	@RequestMapping(value = "calculatedProduct/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	CalculatedProduct deleteCalculatedProduct(@PathVariable Long id) {
		return calculatedProductRepository.delete(id);
	}
}

package ru.galeev.aggrecalor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.repository.CategoryRepository;

import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryRepository repository;

    @RequestMapping(value = "all", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    List<Category> getAll() {
        return repository.getAll();
    }

    @RequestMapping(value = "byId", method = RequestMethod.GET)
    @ResponseBody
    Category get(@RequestParam Long id) {
        return repository.get(id);
    }

    @RequestMapping(value = "byName", method = RequestMethod.GET)
    @ResponseBody
    Category get(@RequestParam String name) {
        return repository.get(name);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    Category create(@RequestBody Category category) {
        return repository.create(category);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Category update(@RequestBody Category category) {
        return repository.update(category);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    Category delete(@PathVariable Long id) {
        return repository.delete(id);
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    Exception handleInvalidDataAccessApiUsageException(InvalidDataAccessApiUsageException ex) {
        return ex;
    }
}
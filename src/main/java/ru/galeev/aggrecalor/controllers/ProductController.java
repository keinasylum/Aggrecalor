package ru.galeev.aggrecalor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.galeev.aggrecalor.entities.Product;
import ru.galeev.aggrecalor.entities.requests.ImportRequest;
import ru.galeev.aggrecalor.repository.ProductRepository;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("product")
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @RequestMapping(value = "all", method = RequestMethod.GET, produces={"application/json; charset=UTF-8"})
    @ResponseBody
    List<Product> getAll() {
        return repository.getAll();
    }

	@RequestMapping(value = "byCategoryId", method = RequestMethod.GET, produces={"application/json; charset=UTF-8"})
	@ResponseBody
	List<Product> getByCategory(@RequestParam Long categoryId) {
		return repository.getByCategoryId(categoryId);
	}

    @RequestMapping(value = "import", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseBody
    Set<Product> importData(@RequestBody ImportRequest request) {
        return repository.importData(request);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    Product create(@RequestBody Product product) {
        return repository.create(product);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    Product update(@RequestBody Product product) {
        return repository.update(product);
    }

    @RequestMapping(value = "{id}",method = RequestMethod.DELETE)
    @ResponseBody
    Product delete(@PathVariable Long id) {
        return repository.delete(id);
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    Exception handleInvalidDataAccessApiUsageException(InvalidDataAccessApiUsageException ex) {
        return ex;
    }
}
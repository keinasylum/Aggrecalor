package ru.galeev.aggrecalor.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping("main")
    String showIndex() {
        return "index";
    }
}

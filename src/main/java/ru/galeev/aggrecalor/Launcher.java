package ru.galeev.aggrecalor;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.webapp.WebAppContext;

public class Launcher {

	public static void main(String[] args) throws Exception {
        Server server = new Server();
        Connector connector = new SelectChannelConnector();
        connector.setPort(8081);
        server.addConnector(connector);
        WebAppContext context = new WebAppContext("src/main/webapp", "/aggrecalor");
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            context.getInitParams().put("org.eclipse.jetty.servlet.Default.useFileMappedBuffer", "false");
        }
        server.setHandler(context);
        server.start();
    }
}
package ru.galeev.aggrecalor.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.entities.requests.validators.RequestValidator;
import ru.galeev.aggrecalor.entities.validators.EntityValidator;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class CategoryRepository {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private EntityValidator entityValidator;

    @PersistenceContext
    private EntityManager manager;

    public Category create(Category category) {
        entityValidator.prePersistValidate(category);
        manager.persist(category);
        manager.flush();
        return category;
    }

    public Category create(String categoryName) {
        requestValidator.validate(categoryName);
        Category category = new Category();
        category.setName(categoryName);
        manager.persist(category);
        manager.flush();
        return category;
    }

    public Category update(Category category) {
        entityValidator.fullValidate(category);
        manager.merge(category);
        manager.flush();
        return category;
    }

	public Category delete(Long id) {
		requestValidator.validate(id);
		Category category = get(id);
		return delete(category);
	}

    public Category delete(Category category) {
        entityValidator.fullValidate(category);
        manager.remove(category);
        manager.flush();
        return category;
    }

    public Category get(String name) {
        requestValidator.validate(name);
        TypedQuery<Category> query = manager.createNamedQuery("Category.getByName", Category.class);
        query.setParameter("name", name);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

	public Category get(Long id) {
		requestValidator.validate(id);
		TypedQuery<Category> query = manager.createNamedQuery("Category.getById", Category.class);
		query.setParameter("id", id);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

    public List<Category> getAll() {
        return manager.createNamedQuery("Category.getAll", Category.class).getResultList();
    }
}

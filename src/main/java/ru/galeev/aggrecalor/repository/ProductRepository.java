package ru.galeev.aggrecalor.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.aggrecalor.entities.Category;
import ru.galeev.aggrecalor.entities.Product;
import ru.galeev.aggrecalor.entities.requests.ImportRequest;
import ru.galeev.aggrecalor.entities.requests.validators.RequestValidator;
import ru.galeev.aggrecalor.entities.validators.EntityValidator;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
@Transactional
public class ProductRepository {

	@Autowired
	private RequestValidator requestValidator;

	@Autowired
	private EntityValidator entityValidator;

    @Autowired
    private CategoryRepository categoryRepository;

	@PersistenceContext
	private EntityManager manager;

    public Product create(Product product) {
        entityValidator.prePersistValidate(product);
		product.setImported(false);
		product.setCreationDate(new Date());
        manager.persist(product);
        manager.flush();
        return product;
    }

    public Product update(Product product) {
        entityValidator.fullValidate(product);
        manager.merge(product);
        manager.flush();
        return product;
    }

	public Product delete(Long id) {
		requestValidator.validate(id);
		Product product = get(id);
		return delete(product);
	}

    public Product delete(Product product) {
        entityValidator.fullValidate(product);
        manager.remove(product);
        manager.flush();
        return product;
    }

	public Product get(Long id) {
		requestValidator.validate(id);
		TypedQuery<Product> query = manager.createNamedQuery("Product.getById", Product.class);
		query.setParameter("id", id);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Set<Product> importData(ImportRequest request) {
		requestValidator.validate(request);
		Category category = categoryRepository.get(request.getCategoryName());
		if (category == null) {
			category = categoryRepository.create(request.getCategoryName());
		}
		Set<Product> products = request.getProducts();
		Date creationDate = new Date();
		for (Product product : products) {
			product.setCategory(category);
			product.setImported(true);
			product.setCreationDate(creationDate);
			manager.persist(product);
		}
		manager.flush();
		return products;
	}

    public List<Product> getAll() {
        return manager.createNamedQuery("Product.getAll", Product.class).getResultList();
    }

	public List<Product> getByCategoryId(Long id) {
		Category category = categoryRepository.get(id);
		return getByCategory(category);
	}

	public List<Product> getByCategory(Category category) {
		entityValidator.fullValidate(category);
		TypedQuery<Product> query = manager.createNamedQuery("Product.getByCategory", Product.class);
		query.setParameter("category", category);
		return query.getResultList();
	}
}
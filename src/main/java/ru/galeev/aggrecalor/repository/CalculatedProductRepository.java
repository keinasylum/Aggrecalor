package ru.galeev.aggrecalor.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.aggrecalor.entities.CalculatedProduct;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
@Transactional
public class CalculatedProductRepository {

	@PersistenceContext
	private EntityManager manager;

	public CalculatedProduct delete(CalculatedProduct calculatedProduct) {
		manager.remove(calculatedProduct);
		manager.flush();
		return calculatedProduct;
	}

	public CalculatedProduct delete(Long id) {
		CalculatedProduct calculatedProduct = get(id);
		return delete(calculatedProduct);
	}

	public CalculatedProduct get(Long id) {
		TypedQuery<CalculatedProduct> query = manager.createNamedQuery("CalculatedProduct.getById", CalculatedProduct.class);
		query.setParameter("id", id);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}

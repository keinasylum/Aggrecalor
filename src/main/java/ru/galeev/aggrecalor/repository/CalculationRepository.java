package ru.galeev.aggrecalor.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.galeev.aggrecalor.entities.Calculation;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class CalculationRepository {

	@PersistenceContext
	private EntityManager manager;

	public Calculation create(Calculation calculation) {
		manager.persist(calculation);
		manager.flush();
		return calculation;
	}

	public Calculation update(Calculation calculation) {
		return null;
	}

	public Calculation delete(Calculation calculation) {
		manager.remove(calculation);
		manager.flush();
		return calculation;
	}

	public Calculation delete(Long id) {
		Calculation calculation = get(id);
		return delete(calculation);
	}

	public List<Calculation> getAll() {
		return manager.createNamedQuery("Calculation.getAll", Calculation.class).getResultList();
	}

	public Calculation get(Long id) {
		TypedQuery<Calculation> query = manager.createNamedQuery("Calculation.getById", Calculation.class);
		query.setParameter("id", id);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
